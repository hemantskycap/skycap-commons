package skycap.ratingview;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.TypedArray;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

/**
 * RatingView provides you the view that can be customized dynamically in your Java code
 * and in xml according to your app theme and you can also show and hide RatingView after an interval of days.
 *
 * @author Chirag
 *         Created on 02/01/18.
 */

public class RatingView extends FrameLayout {
    private static final String TAG = "RatingView";
    private static final String RATING_VIEW_PREFS = "rating_view_prefs";
    private static final String KEY_LAST_VISIBLE_TIME = "key_last_visible_time";
    private static final long MILLIS_IN_A_DAY = TimeUnit.DAYS.toMillis(1);
    //    private static final long MILLIS_IN_A_DAY = 20000;    //20 seconds
    private static final int DEFAULT_DAYS_GAP = 3;

    private int nextVisibleDays = 0;

    private AppCompatTextView rate;
    private AppCompatTextView later;
    private AppCompatTextView rateContent;
    private AppCompatImageView rateLogo;
    private CardView rateCardView;

    private SharedPreferences sharedPreferences;

    /**
     * @param context reference of current view
     * @param attrs   custom properties for ratingView
     */
    public RatingView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    /**
     * @param context      reference of current view
     * @param attrs        custom properties for ratingView
     * @param defStyleAttr style for ratingView
     */
    public RatingView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    /**
     * @param context reference of current view
     * @param attrs   custom properties for ratingView
     */
    private void init(Context context, AttributeSet attrs) {
        View view = inflate(context, R.layout.app_rating_view, this);

        rate = view.findViewById(R.id.textView_rate_app);
        later = view.findViewById(R.id.textView_later);
        rateContent = view.findViewById(R.id.textView_rate_app_text);

        rateLogo = view.findViewById(R.id.imageView_app_icon);
        rateCardView = view.findViewById(R.id.rateCardView);

        rate.setOnClickListener(v -> {
            try {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("market://details?id=" + context.getPackageName()));
                context.startActivity(intent);
                extendTime();

            } catch (Exception e) {
                try {
                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    intent.setData(Uri.parse("https://play.google.com/store/apps/details?=" + context.getPackageName()));
                    context.startActivity(Intent.createChooser(intent, "Open link using..."));
                    extendTime();

                } catch (Exception ignored) {
                    Log.e(TAG, "error in starting activity", ignored);
                    Toast.makeText(context, "Error in opening app link :(", Toast.LENGTH_SHORT).show();
                }
            }
        });
        later.setOnClickListener(v -> extendTime());

        TypedArray typedArray = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.RatingView,
                0, 0);
        try {

            int rateContentTextColor = typedArray.getInt(R.styleable.RatingView_rv_rateContentTextColor, 0);
            if (rateContentTextColor != 0) {
                setRateContentTextColor(rateContentTextColor);
            }

            int logoTintColor = typedArray.getInt(R.styleable.RatingView_rv_rateLogoTintColor, 0);
            if (logoTintColor != 0) {
                setRateLogoTintColor(logoTintColor);
            }

            int rateButtonTextColor = typedArray.getInt(R.styleable.RatingView_rv_rateButtonTextColor, 0);
            if (rateButtonTextColor != 0) {
                setRateButtonTextColor(rateButtonTextColor);
            }

            int laterButtonTextColor = typedArray.getInt(R.styleable.RatingView_rv_laterButtonTextColor, 0);
            if (laterButtonTextColor != 0) {
                setLaterButtonTextColor(laterButtonTextColor);
            }

            int cardBackgroundColor = typedArray.getInt(R.styleable.RatingView_rv_ratingBackgroundColor, 0);
            if (cardBackgroundColor != 0) {
                setCardBackgroundColor(cardBackgroundColor);
            }

            float cardCornerRadius = typedArray.getDimension(R.styleable.RatingView_rv_cardCornerRadius, 0);
            if (cardCornerRadius != 0) {
                setCardCornerRadius((int) cardCornerRadius);
            }

            float cardElevation = typedArray.getDimension(R.styleable.RatingView_rv_cardElevation, 0);
            if (cardElevation != 0) {
                setCardElevation((int) cardElevation);
            }

            float cardMaxElevation = typedArray.getDimension(R.styleable.RatingView_rv_cardMaxElevation, 0);
            if (cardMaxElevation != 0) {
                setCardMaxElevation((int) cardMaxElevation);
            }

            boolean useCardCompatPadding = typedArray.getBoolean(R.styleable.RatingView_rv_useCardCompatPadding, true);
            setUseCardCompatPadding(useCardCompatPadding);

            Drawable rateLogo = typedArray.getDrawable(R.styleable.RatingView_rv_rateLogo);
            if (rateLogo != null) {
                setRateLogo(rateLogo);
            }

            String rateButtonText = typedArray.getString(R.styleable.RatingView_rv_rateButtonText);
            if (rateButtonText != null) {
                setRateButtonText(rateButtonText);
            }

            String rateButtonTextFont = typedArray.getString(R.styleable.RatingView_rv_rateButtonTextFont);
            if (rateButtonTextFont != null) {
                setRateButtonFont(Typeface.create(rateButtonTextFont, Typeface.BOLD_ITALIC));
            }

            Drawable rateButtonDrawable = typedArray.getDrawable(R.styleable.RatingView_rv_rateButtonDrawable);
            if (rateButtonDrawable != null) {
                setRateButtonDrawable(rateButtonDrawable);
            }

            String laterButtonText = typedArray.getString(R.styleable.RatingView_rv_laterButtonText);
            if (laterButtonText != null) {
                setLaterButtonText(laterButtonText);
            }

            String laterButtonTextFont = typedArray.getString(R.styleable.RatingView_rv_laterButtonTextFont);
            if (laterButtonTextFont != null) {
                setLaterButtonFont(Typeface.create(laterButtonTextFont, Typeface.BOLD_ITALIC));
            }

            String rateContentTextFont = typedArray.getString(R.styleable.RatingView_rv_rateContentTextFont);
            if (rateContentTextFont != null) {
                setRateContentFont(Typeface.create(rateContentTextFont, Typeface.BOLD_ITALIC));
            }

            Drawable laterButtonDrawable = typedArray.getDrawable(R.styleable.RatingView_rv_laterButtonDrawable);
            if (laterButtonDrawable != null) {
                setLaterButtonDrawable(laterButtonDrawable);
            }

            String rateContentText = typedArray.getString(R.styleable.RatingView_rv_rateContentText);
            if (rateContentText != null) {
                setRateContentText(rateContentText);
            }

            nextVisibleDays = typedArray.getInt(R.styleable.RatingView_rv_nextVisibleDays, DEFAULT_DAYS_GAP);

        } finally {
            typedArray.recycle();
        }

        sharedPreferences = context.getSharedPreferences(RATING_VIEW_PREFS, Context.MODE_PRIVATE);
        updateVisibility();
    }

    /**
     * updates visibility of rating view
     */
    private void updateVisibility() {
        long lastVisibleTime = sharedPreferences.getLong(KEY_LAST_VISIBLE_TIME, 0);

        if (lastVisibleTime == 0) {
            extendTime();

        } else {
            long currentTimeMillis = System.currentTimeMillis();
            long nextVisibleTime = getNextVisibleTime();
            long time = currentTimeMillis - nextVisibleTime;
            if (lastVisibleTime > time) {
                setVisibility(GONE);
            }
        }
    }

    /**
     * @return time after which the rating view will be visible
     */
    private long getNextVisibleTime() {
        return MILLIS_IN_A_DAY * nextVisibleDays;
    }

    /**
     * extend interval of days after which rating view will be visible
     */
    private void extendTime() {
        sharedPreferences.edit()
                .putLong(KEY_LAST_VISIBLE_TIME, System.currentTimeMillis())
                .apply();
        setVisibility(GONE);
    }

    /**
     * set background color for rating view.
     *
     * @param color set as card background
     */
    public void setCardBackgroundColor(int color) {
        rateCardView.setCardBackgroundColor(color);
    }

    /**
     * set elevation for rating view
     *
     * @param elevation value that gives shadow effect to the card
     */
    public void setCardElevation(int elevation) {
        rateCardView.setCardElevation(elevation);
    }

    /**
     * set max elevation for rating view
     *
     * @param maxElevation value that gives max shadow effect to the card
     */
    public void setCardMaxElevation(int maxElevation) {
        rateCardView.setMaxCardElevation(maxElevation);
    }

    /**
     * set corner radius for rating view
     *
     * @param cardCornerRadius value that sets as card corner radius
     */
    public void setCardCornerRadius(int cardCornerRadius) {
        rateCardView.setRadius(cardCornerRadius);
    }

    /**
     * set default cardPadding for ratingView
     *
     * @param useCardCompatPadding boolean value to set default cardPadding on rating view
     */
    public void setUseCardCompatPadding(boolean useCardCompatPadding) {
        rateCardView.setUseCompatPadding(useCardCompatPadding);
    }

    /**
     * set logo for rating view
     *
     * @param logo drawable for rate logo
     */
    public void setRateLogo(Drawable logo) {
        rateLogo.setImageDrawable(logo);
    }

    /**
     * set tint on rateLogo
     *
     * @param rateLogoTintColor tint color on rate logo
     */
    public void setRateLogoTintColor(int rateLogoTintColor) {
        rateLogo.setColorFilter(rateLogoTintColor, PorterDuff.Mode.SRC_ATOP);
    }

    /**
     * set text for rateContent
     *
     * @param rateTextContent text for rateTextContent
     */
    public void setRateContentText(String rateTextContent) {
        rateContent.setText(rateTextContent);
    }

    /**
     * set text color for rate content
     *
     * @param rateContentTextColor text color for rateContentText
     */
    public void setRateContentTextColor(int rateContentTextColor) {
        rateContent.setTextColor(rateContentTextColor);
    }

    /**
     * set font for rateContent
     *
     * @param typeface font for rateContent
     */
    public void setRateContentFont(Typeface typeface) {
        rateContent.setTypeface(typeface);
    }

    /**
     * set text for rateButton
     *
     * @param rateButtonText text for rateButton
     */
    public void setRateButtonText(String rateButtonText) {
        rate.setText(rateButtonText);
    }

    /**
     * set text color for rateButton
     *
     * @param rateButtonTextColor textColor for rateButton
     */
    public void setRateButtonTextColor(int rateButtonTextColor) {
        rate.setTextColor(rateButtonTextColor);
    }

    /**
     * set font for rateButton
     *
     * @param typeface font for rateButton
     */
    public void setRateButtonFont(Typeface typeface) {
        rate.setTypeface(typeface);
    }

    /**
     * set drawable for rate button
     *
     * @param rateButtonDrawable drawable for rateButton
     */
    public void setRateButtonDrawable(Drawable rateButtonDrawable) {
        rate.setBackgroundDrawable(rateButtonDrawable);
    }

    /**
     * set text for later button
     *
     * @param laterButtonText text for laterButton
     */
    public void setLaterButtonText(String laterButtonText) {
        later.setText(laterButtonText);
    }

    /**
     * set text color for later button
     *
     * @param laterButtonTextColor textColor for laterButton
     */
    public void setLaterButtonTextColor(int laterButtonTextColor) {
        later.setTextColor(laterButtonTextColor);
    }

    /**
     * set drawable for later button
     *
     * @param laterButtonDrawable drawable for laterButton
     */
    public void setLaterButtonDrawable(Drawable laterButtonDrawable) {
        later.setBackgroundDrawable(laterButtonDrawable);
    }

    /**
     * set font for laterButton
     *
     * @param typeface font for laterButton
     */
    public void setLaterButtonFont(Typeface typeface) {
        later.setTypeface(typeface);
    }

    /**
     * set an interval of days after which rating view will be visible
     *
     * @param days after which rating view will be visible
     */
    public void setVisibleInterval(int days) {
        nextVisibleDays = days;
        updateVisibility();
    }

}
